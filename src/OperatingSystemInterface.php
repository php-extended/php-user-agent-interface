<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use PhpExtended\Version\VersionInterface;
use Stringable;

/**
 * OperatingSystemInterface interface file.
 * 
 * This interface represents an operating system that emits an user agent.
 * 
 * @author Anastaszor
 */
interface OperatingSystemInterface extends Stringable
{
	
	/**
	 * Gets the name of the operating system.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the version number of this operating system.
	 * 
	 * @return VersionInterface
	 */
	public function getVersion() : VersionInterface;
	
	/**
	 * Gets the family of this operating system.
	 * 
	 * @return OperatingSystemFamilyInterface
	 */
	public function getFamily() : OperatingSystemFamilyInterface;
	
	/**
	 * Gets whether this operating system equals the other given operating
	 * system.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $operatingSystem
	 * @return boolean
	 */
	public function equals($operatingSystem) : bool;
	
}
