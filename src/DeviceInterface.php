<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use Stringable;

/**
 * DeviceInterface interface file.
 * 
 * This interface represents a device from which an user agent is emitted.
 * 
 * @author Anastaszor
 */
interface DeviceInterface extends Stringable
{
	
	/**
	 * Gets the name of the device.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the type of device.
	 * 
	 * @return DeviceTypeInterface
	 */
	public function getDeviceType() : DeviceTypeInterface;
	
	/**
	 * Gets the brand of the device.
	 * 
	 * @return BrandInterface
	 */
	public function getBrand() : BrandInterface;
	
	/**
	 * Gets whether this device equals the other given device.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $device
	 * @return boolean
	 */
	public function equals($device) : bool;
	
}
