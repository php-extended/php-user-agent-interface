<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use Stringable;

/**
 * BrandInterface interface file.
 * 
 * This interface represents a brand that builds devices.
 * 
 * @author Anastaszor
 */
interface BrandInterface extends Stringable
{
	
	/**
	 * Gets the name of the brand.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets whether this brand equals the other given brand.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $brand
	 * @return boolean
	 */
	public function equals($brand) : bool;
	
}
