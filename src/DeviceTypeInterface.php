<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use Stringable;

/**
 * DeviceTypeInterface interface file.
 * 
 * This interface represents a type of a given device.
 * 
 * @author Anastaszor
 */
interface DeviceTypeInterface extends Stringable
{
	
	/**
	 * Gets the name of the device type.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets whether this device type equals the other given device type.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $type
	 * @return boolean
	 */
	public function equals($type) : bool;
	
}
