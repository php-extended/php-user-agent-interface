<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use Stringable;

/**
 * UserAgentTypeInterface interface file.
 * 
 * This interface represents the type of this user agent.
 * 
 * @author Anastaszor
 */
interface UserAgentTypeInterface extends Stringable
{
	
	/**
	 * Gets the name of the user agent type.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets whether this user agent type equals another user agent type.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $type
	 * @return boolean
	 */
	public function equals($type) : bool;
	
}
