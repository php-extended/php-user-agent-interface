<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use DateTimeInterface;
use PhpExtended\Version\VersionInterface;
use Stringable;

/**
 * UserAgentInterface interface file.
 * 
 * This interface describes an user agent.
 * 
 * @author Anastaszor
 */
interface UserAgentInterface extends Stringable
{
	
	/**
	 * Gets the name of this user agent.
	 * 
	 * @return ?string
	 */
	public function getName() : ?string;
	
	/**
	 * Gets the type of this user agent.
	 * 
	 * @return ?UserAgentTypeInterface
	 */
	public function getType() : ?UserAgentTypeInterface;
	
	/**
	 * Gets the version of this user agent.
	 * 
	 * @return ?VersionInterface
	 */
	public function getVersion() : ?VersionInterface;
	
	/**
	 * Gets the device this user agent runs on.
	 * 
	 * @return ?DeviceInterface
	 */
	public function getDevice() : ?DeviceInterface;
	
	/**
	 * Gets the operating system of this user agent.
	 * 
	 * @return ?OperatingSystemInterface
	 */
	public function getOperatingSystem() : ?OperatingSystemInterface;
	
	/**
	 * Gets the rendering engine of this user agent.
	 * 
	 * @return ?RenderingEngineInterface
	 */
	public function getRenderingEngine() : ?RenderingEngineInterface;
	
	/**
	 * Gets the date when this user agent was used on the internet.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getApparitionDate() : ?DateTimeInterface;
	
	/**
	 * Gets the date after which the market share of this user agent become
	 * near zero (relative to its peak market share).
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDisparitionDate() : ?DateTimeInterface;
	
	/**
	 * Gets a suitable representation for the User-Agent http header.
	 * 
	 * @return string
	 */
	public function getHeaderValue() : string;
	
	/**
	 * Gets whether this user agent equals another user agent.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $other
	 * @return boolean
	 */
	public function equals($other) : bool;
	
}
