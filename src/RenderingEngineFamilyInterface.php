<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use Stringable;

/**
 * RenderingEngineFamilyInterface interface file.
 * 
 * This interface describes the families of rendering engines.
 * 
 * @author Anastaszor
 */
interface RenderingEngineFamilyInterface extends Stringable
{
	
	/**
	 * Gets the name of this rendering engine family.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets whether this family equals the other given rendering engine family.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $family
	 * @return boolean
	 */
	public function equals($family) : bool;
	
}
