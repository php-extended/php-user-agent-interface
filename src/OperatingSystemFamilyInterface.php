<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use Stringable;

/**
 * OperatingSystemFamilyInterface interface file.
 * 
 * This interface represents the family of an operating system.
 * 
 * @author Anastaszor
 */
interface OperatingSystemFamilyInterface extends Stringable
{
	
	/**
	 * Gets the name of this operating system family.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the brand that provides this operating system.
	 * 
	 * @return BrandInterface
	 */
	public function getBrand() : BrandInterface;
	
	/**
	 * Gets whether this family equals to the given family.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $family
	 * @return boolean
	 */
	public function equals($family) : bool;
	
}
