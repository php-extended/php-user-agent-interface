<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-user-agent-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\UserAgent;

use PhpExtended\Version\VersionInterface;
use Stringable;

/**
 * RenderingEngineInterface interface file.
 * 
 * This interface represents a rendering engine that emits an user agent.
 * 
 * @author Anastaszor
 */
interface RenderingEngineInterface extends Stringable
{
	
	/**
	 * Gets the name of this rendering engine.
	 * 
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the version number of this rendering engine.
	 * 
	 * @return VersionInterface
	 */
	public function getVersion() : VersionInterface;
	
	/**
	 * Gets the family of this rendering engine.
	 * 
	 * @return RenderingEngineFamilyInterface
	 */
	public function getFamily() : RenderingEngineFamilyInterface;
	
	/**
	 * Gets whether this rendering engine equals the other given rendering
	 * engine.
	 * 
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $renderingEngine
	 * @return boolean
	 */
	public function equals($renderingEngine) : bool;
	
}
